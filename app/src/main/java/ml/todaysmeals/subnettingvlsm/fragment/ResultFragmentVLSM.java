package ml.todaysmeals.subnettingvlsm.fragment;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import ml.todaysmeals.subnettingvlsm.R;
import ml.todaysmeals.subnettingvlsm.classes.SubnetBlock;

/**
 * Created by GHOSH on 3/22/2017.
 */

public class ResultFragmentVLSM extends DialogFragment {
    private AlertDialog.Builder builder;
    private TableLayout resultTable;
    private TableRow resultRow;
    private List<SubnetBlock> subnetBlockList;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.result_dialog_vlsm, null);
        resultTable = (TableLayout) view.findViewById(R.id.vlsmResultTable);
        builder.setView(view);
        Dialog dialog = builder.create();
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        subnetBlockList = (List<SubnetBlock>) bundle.getSerializable(VLSM.VLSM_LIST);
        showResultTable(subnetBlockList);
    }

    private void showResultTable(List<SubnetBlock> subnetBlockList) {
        TextView hostTextView;
        TextView networkIDTextView;
        TextView firstHostTextView;
        TextView lastHostTextView;
        TextView broadcastTextView;
        TextView cidrTextView;
        TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,TableRow.LayoutParams.WRAP_CONTENT
        );
        int margin = 5;
        int font_size = 8;
        textViewParams.setMargins(margin,margin,margin,margin);

        for (int index = 0; index < subnetBlockList.size(); index++) {
            hostTextView = new TextView(getContext());
            hostTextView.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP,font_size));
          //  hostTextView.setPadding(10,10,10,10);
            hostTextView.setBackgroundColor(getResources().getColor(R.color.color3));

            networkIDTextView = new TextView(getContext());
            networkIDTextView.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP,font_size));
            networkIDTextView.setBackgroundColor(getResources().getColor(R.color.color3));

            firstHostTextView = new TextView(getContext());
            firstHostTextView.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP,font_size));
            firstHostTextView.setBackgroundColor(getResources().getColor(R.color.color3));

            lastHostTextView = new TextView(getContext());
            lastHostTextView.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP,font_size));
            lastHostTextView.setBackgroundColor(getResources().getColor(R.color.color3));

            broadcastTextView = new TextView(getContext());
            broadcastTextView.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP,font_size));
            broadcastTextView.setBackgroundColor(getResources().getColor(R.color.color3));

            cidrTextView = new TextView(getContext());
            cidrTextView.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP,font_size));
            cidrTextView.setBackgroundColor(getResources().getColor(R.color.color3));

            resultRow = new TableRow(getContext());


            hostTextView.setText(subnetBlockList.get(index).hostNumber + "");
            networkIDTextView.setText(subnetBlockList.get(index).subnets.getNetworkID());
            firstHostTextView.setText(subnetBlockList.get(index).subnets.getFirstHost());
            lastHostTextView.setText(subnetBlockList.get(index).subnets.getLastHost());
            broadcastTextView.setText(subnetBlockList.get(index).subnets.getBreoadCastAddress());
            cidrTextView.setText(subnetBlockList.get(index).subnets.getCidrVALUE() + "");

            resultRow.addView(hostTextView,textViewParams);
            resultRow.addView(networkIDTextView,textViewParams);
            resultRow.addView(firstHostTextView,textViewParams);
            resultRow.addView(lastHostTextView,textViewParams);
            resultRow.addView(broadcastTextView,textViewParams);
            resultRow.addView(cidrTextView,textViewParams);

            resultTable.addView(resultRow);
        }
    }

    int getPixels(int unit, float size) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return (int) TypedValue.applyDimension(unit, size, metrics);
    }
}
