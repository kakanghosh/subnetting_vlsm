package ml.todaysmeals.subnettingvlsm.fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ml.todaysmeals.subnettingvlsm.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HostVLSMDialog extends DialogFragment implements View.OnClickListener{
    private AlertDialog.Builder builder;
    private EditText hostNumberEditText;
    private Button cancelDialogButton;
    private Button addHostButton;
    private HostdialogInterface hostdialogInterface;

    public HostVLSMDialog() {
        // Required empty public constructor
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_host_vlsm_dialog,null);
        hostNumberEditText = (EditText) view.findViewById(R.id.addHostEditText);
        cancelDialogButton = (Button) view.findViewById(R.id.cancelHostInput);
        addHostButton = (Button) view.findViewById(R.id.addHostButton);

        builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);
        builder.setView(view);
        setRetainInstance(true);
        Dialog dialog = builder.create();
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        cancelDialogButton.setOnClickListener(this);
        addHostButton.setOnClickListener(this);
    }

    public void attachFragment(VLSM vlsm){
        hostdialogInterface = vlsm;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cancelHostInput){
            hostdialogInterface.hideDialog();
        }else if (view.getId() == R.id.addHostButton){
            if (hostNumberEditText.length() > 0){
                int hostNumber = Integer.parseInt(hostNumberEditText.getText().toString());
                hostdialogInterface.addHostToList(hostNumber);
                hostNumberEditText.setText("");
            }
        }
    }

    public interface HostdialogInterface{
        void hideDialog();
        void addHostToList(int host);
    }
}
