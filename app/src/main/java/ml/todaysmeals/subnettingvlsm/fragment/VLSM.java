package ml.todaysmeals.subnettingvlsm.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

import ml.todaysmeals.subnettingvlsm.R;
import ml.todaysmeals.subnettingvlsm.classes.ClassAVLSM;
import ml.todaysmeals.subnettingvlsm.classes.ClassBVLSM;
import ml.todaysmeals.subnettingvlsm.classes.ClassCVLSM;
import ml.todaysmeals.subnettingvlsm.classes.WatchEditText;
import ml.todaysmeals.subnettingvlsm.debug.LogMessage;
import ml.todaysmeals.subnettingvlsm.debug.MyToast;
import ml.todaysmeals.subnettingvlsm.debug.SoftkeyBoard;

/**
 * A simple {@link Fragment} subclass.
 */
public class VLSM extends Fragment implements View.OnClickListener, HostVLSMDialog.HostdialogInterface {
    private ArrayList<Integer> listOfHost;
    private EditText firstOctateEditText;
    private EditText secondOctateEditText;
    private EditText thirdOctateEditText;
    private EditText forthOctateEditText;
    private EditText cidrOctateEditText;
    private Button inputHostButton;
    private Button calculateVLSMButton;
    private Button clearHostButton;
    private TextView hostListTextView;
    private boolean flag;
    private HostVLSMDialog hostVLSMDialog;
    private int counter;
    private ResultFragmentVLSM resultFragmentVLSM;
    private Bundle bundle;
    private WatchEditText.OctateText firstOctateTextWatcher;
    private WatchEditText.OctateText secondOctateTextWatcher;
    private WatchEditText.OctateText thirdOctateTextWatcher;
    private WatchEditText.OctateText forthOctateTextWatcher;
    private WatchEditText.OctateText cidrTextWatcher;
    public static final String HOST_DIALOG = "host_dialog";
    public static final String RESULT_DIALOG = "result_dialog";
    public static final String VLSM_LIST = "vlsm_list";

    public VLSM() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vlsm, container, false);
        // Binding the widget by widget ID
        firstOctateEditText = (EditText) view.findViewById(R.id.firstOctateVLSM);
        secondOctateEditText = (EditText) view.findViewById(R.id.secondOctateVLSM);
        thirdOctateEditText = (EditText) view.findViewById(R.id.thirdOctateVLSM);
        forthOctateEditText = (EditText) view.findViewById(R.id.forthOctateVLSM);
        cidrOctateEditText = (EditText) view.findViewById(R.id.cidrEditTextVLSM);
        inputHostButton = (Button) view.findViewById(R.id.enterHostButton);
        calculateVLSMButton = (Button) view.findViewById(R.id.calculateVLSM);
        clearHostButton = (Button) view.findViewById(R.id.clearHostNumber);
        hostListTextView = (TextView) view.findViewById(R.id.showHostNumber);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listOfHost = new ArrayList<>();
        inputHostButton.setOnClickListener(this);
        calculateVLSMButton.setOnClickListener(this);
        clearHostButton.setOnClickListener(this);
        hostVLSMDialog = new HostVLSMDialog();
        bundle = new Bundle();
        firstOctateTextWatcher = new WatchEditText.OctateText(0,255,firstOctateEditText);
        secondOctateTextWatcher = new WatchEditText.OctateText(0,255,secondOctateEditText);
        thirdOctateTextWatcher = new WatchEditText.OctateText(0,255,thirdOctateEditText);
        forthOctateTextWatcher = new WatchEditText.OctateText(0,255,forthOctateEditText);
        cidrTextWatcher = new WatchEditText.OctateText(0,32,cidrOctateEditText);

        firstOctateEditText.addTextChangedListener(firstOctateTextWatcher);
        secondOctateEditText.addTextChangedListener(secondOctateTextWatcher);
        thirdOctateEditText.addTextChangedListener(thirdOctateTextWatcher);
        forthOctateEditText.addTextChangedListener(forthOctateTextWatcher);
        cidrOctateEditText.addTextChangedListener(cidrTextWatcher);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.enterHostButton) {
            hostVLSMDialog.attachFragment(this);
            hostVLSMDialog.show(getFragmentManager(), HOST_DIALOG);
            hostVLSMDialog.setCancelable(false);
            LogMessage.debug("Enter Host Button Clicked!");
        } else if (view.getId() == R.id.calculateVLSM) {
            LogMessage.debug("Calculate VLSM Button Clicked!");
            int firstOctate, secondOcate, thirdOctate, forthOctate, cidrValue;
            if (firstOctateEditText.getText().length() > 0 && secondOctateEditText.getText().length() > 0 &&
                    thirdOctateEditText.getText().length() > 0 && forthOctateEditText.getText().length() > 0 &&
                    cidrOctateEditText.getText().length() > 0) {
                firstOctate = Integer.parseInt(firstOctateEditText.getText().toString());
                secondOcate = Integer.parseInt(secondOctateEditText.getText().toString());
                thirdOctate = Integer.parseInt(thirdOctateEditText.getText().toString());
                forthOctate = Integer.parseInt(forthOctateEditText.getText().toString());
                cidrValue = Integer.parseInt(cidrOctateEditText.getText().toString());
                if (firstOctate >= 192 && firstOctate <= 223){
                    LogMessage.debug(firstOctate+"."+secondOcate+"."+thirdOctate+"."+forthOctate+"/"+cidrValue);
                    if (listOfHost.size() != 0){
                        ClassCVLSM classCVLSM = new ClassCVLSM(this,listOfHost,firstOctate,secondOcate,thirdOctate,forthOctate,cidrValue);
                        classCVLSM.calculteVLSM();
                        classCVLSM.showVLSM();
                        if (classCVLSM.getListOfVLSM() == null){
                            MyToast.Message("VLSM IS NOT Possible");
                            return;
                        }
                        resultFragmentVLSM = new ResultFragmentVLSM();
                        bundle.putSerializable(VLSM_LIST, (Serializable) classCVLSM.getListOfVLSM());
                        resultFragmentVLSM.setArguments(bundle);
                        resultFragmentVLSM.show(getFragmentManager(),RESULT_DIALOG);

                    }else {
                        MyToast.Message("Please Enter Host Number");
                    }
                }else  if (firstOctate >= 128 && firstOctate <= 191){
                    if (listOfHost.size() != 0){
                        ClassBVLSM classBVLSM = new ClassBVLSM(this,listOfHost,firstOctate,secondOcate,thirdOctate,forthOctate,cidrValue);
                        classBVLSM.calculateVLSM();

                        if (classBVLSM.getListOfVLSM() == null){
                            MyToast.Message("VLSM IS NOT Possible");
                            return;
                        }
                        resultFragmentVLSM = new ResultFragmentVLSM();
                        bundle.putSerializable(VLSM_LIST, (Serializable) classBVLSM.getListOfVLSM());
                        resultFragmentVLSM.setArguments(bundle);
                        resultFragmentVLSM.show(getFragmentManager(),RESULT_DIALOG);
                    }
                }else if (firstOctate >= 0 && firstOctate <= 127){
                    if (listOfHost.size() != 0){
                        ClassAVLSM classAVLSM = new ClassAVLSM(this,listOfHost,firstOctate,secondOcate,thirdOctate,forthOctate,cidrValue);
                        classAVLSM.calculateVLSM();
                        if (classAVLSM.getListOfVLSM() == null){
                            MyToast.Message("VLSM IS NOT Possible");
                            return;
                        }
                        resultFragmentVLSM = new ResultFragmentVLSM();
                        bundle.putSerializable(VLSM_LIST, (Serializable) classAVLSM.getListOfVLSM());
                        resultFragmentVLSM.setArguments(bundle);
                        resultFragmentVLSM.show(getFragmentManager(),RESULT_DIALOG);
                    }
                }else {
                    Toast.makeText(getContext(),"Enter Network Between class A - Class C\n" +
                            "First Octate \nClass A: 0 - 127 \nClass B: 128 - 191 \nClass C: 192 - 223",Toast.LENGTH_LONG).show();
                }
            }else {
                MyToast.Message("Please Fill the network Address");
            }
            SoftkeyBoard.hideKeyboardFrom(getContext(),view);
        }else if (view.getId() == R.id.clearHostNumber){
            hostListTextView.setText("");
            listOfHost.clear();
            counter = 0;
            flag = false;
            MyToast.Message("Clear All Host");
        }
    }

    @Override
    public void hideDialog() {
        hostVLSMDialog.dismiss();
    }

    @Override
    public void addHostToList(int host) {
        listOfHost.add(host);
        LogMessage.debug(host + " Added to List!");
        if (!flag) {
            hostListTextView.setText("Lis Of Host\n");
            flag = true;
        }
        hostListTextView.setText(hostListTextView.getText().toString() + "\n" + ++counter + ". " + host);
        MyToast.Message( host + " Added to List!");
    }

}
