package ml.todaysmeals.subnettingvlsm.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;

import ml.todaysmeals.subnettingvlsm.R;
import ml.todaysmeals.subnettingvlsm.classes.ClassA;
import ml.todaysmeals.subnettingvlsm.classes.ClassB;
import ml.todaysmeals.subnettingvlsm.classes.ClassC;
import ml.todaysmeals.subnettingvlsm.classes.Subnets;
import ml.todaysmeals.subnettingvlsm.classes.WatchEditText;
import ml.todaysmeals.subnettingvlsm.debug.LogMessage;
import ml.todaysmeals.subnettingvlsm.debug.MyToast;
import ml.todaysmeals.subnettingvlsm.debug.SoftkeyBoard;

/**
 * A simple {@link Fragment} subclass.
 */
public class Subnetting extends Fragment implements View.OnClickListener {
    private EditText firstOctate;
    private EditText secondOctate;
    private EditText thirdOctate;
    private EditText forthOctate;
    private EditText cidr;
    private EditText noOfHost;
    private Button calculateSubnet;
    private Bundle bundle;
    private ResultFragmentSubnetting resultFragmentSubnetting;
    private ClassA classA;
    private ClassB classB;
    private ClassC classC;
    private BackGroundWork backGroundWork;
    private WatchEditText.OctateText firstOctateTextWatcher;
    private WatchEditText.OctateText secondOctateTextWatcher;
    private WatchEditText.OctateText thirdOctateTextWatcher;
    private WatchEditText.OctateText forthOctateTextWatcher;
    private WatchEditText.OctateText cidrTextWatcher;
    private WatchEditText.OctateText hostTextWatcher;
    public final static String SUBNET_LIST = "subnets";
    public final static String RESULT_FRAGMENT = "result_fragmnet";

    public Subnetting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subnetting, container, false);
        firstOctate = (EditText) view.findViewById(R.id.firstOctate);
        secondOctate = (EditText) view.findViewById(R.id.secondOctate);
        thirdOctate = (EditText) view.findViewById(R.id.thirdOctate);
        forthOctate = (EditText) view.findViewById(R.id.forthOctate);
        cidr = (EditText) view.findViewById(R.id.cidrEditText);
        noOfHost = (EditText) view.findViewById(R.id.noOfhostEditText);
        calculateSubnet = (Button) view.findViewById(R.id.calculateSubnetting);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        firstOctateTextWatcher = new WatchEditText.OctateText(0,255,firstOctate);
        secondOctateTextWatcher = new WatchEditText.OctateText(0,255,secondOctate);
        thirdOctateTextWatcher = new WatchEditText.OctateText(0,255,thirdOctate);
        forthOctateTextWatcher = new WatchEditText.OctateText(0,255,forthOctate);
        cidrTextWatcher = new WatchEditText.OctateText(0,32,cidr);
        hostTextWatcher = new WatchEditText.OctateText(0,Integer.MAX_VALUE,noOfHost);

        firstOctate.addTextChangedListener(firstOctateTextWatcher);
        secondOctate.addTextChangedListener(secondOctateTextWatcher);
        thirdOctate.addTextChangedListener(thirdOctateTextWatcher);
        forthOctate.addTextChangedListener(forthOctateTextWatcher);
        cidr.addTextChangedListener(cidrTextWatcher);
        noOfHost.addTextChangedListener(hostTextWatcher);


        calculateSubnet.setOnClickListener(this);
        bundle = new Bundle();
        resultFragmentSubnetting = new ResultFragmentSubnetting();
        SoftkeyBoard.hideKeyboardFrom(getContext(), getView());
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.calculateSubnetting) {
            if (!isRequiredFieldEmpty(firstOctate.getText().toString(), secondOctate.getText().toString(),
                    thirdOctate.getText().toString(), forthOctate.getText().toString(), cidr.getText().toString(),
                    noOfHost.getText().toString())) {
                int first, second, third, forth, cidrValue, host;
                first = Integer.parseInt(firstOctate.getText().toString());
                second = Integer.parseInt(secondOctate.getText().toString());
                third = Integer.parseInt(thirdOctate.getText().toString());
                forth = Integer.parseInt(forthOctate.getText().toString());
                cidrValue = Integer.parseInt(cidr.getText().toString());
                host = Integer.parseInt(noOfHost.getText().toString());
                LogMessage.debug(firstOctate.getText().toString() + "." + secondOctate.getText().toString() + "." + thirdOctate.getText().toString() + "." + forthOctate.getText().toString() + "/" + cidr.getText().toString());
                if (first >= 0 && first <= 127) {
                    classA = new ClassA(this, host, first, second, third, forth, cidrValue);
                    if (classA.validateNetworkAddress()) {
                        SoftkeyBoard.hideKeyboardFrom(getContext(), view);
                        LogMessage.debug("Class A: " + Subnets.getSubnetMask());
                        backGroundWork = new BackGroundWork();
                        backGroundWork.execute("CLASS-A");
                        //   showResultTable(classA.getListOfSubnet());
                    } else {
                        SoftkeyBoard.hideKeyboardFrom(getContext(), view);
                        //    hideResultTable();
                    }
                } else if (first >= 128 && first <= 191) {
                    classB = new ClassB(this, host, first, second, third, forth, cidrValue);
                    if (classB.validateNetworkAddress()) {
                        SoftkeyBoard.hideKeyboardFrom(getContext(), view);
                        LogMessage.debug("Class B: " + Subnets.getSubnetMask());
                        backGroundWork = new BackGroundWork();
                        backGroundWork.execute("CLASS-B");
                        //    showResultTable(classB.getListOfSubnet());
                    } else {
                        SoftkeyBoard.hideKeyboardFrom(getContext(), view);
                        // hideResultTable();
                    }
                } else if (first >= 192 && first <= 223) {

                    classC = new ClassC(this, host, first, second, third, forth, cidrValue);
                    if (classC.validateNetworkID()) {
                        SoftkeyBoard.hideKeyboardFrom(getContext(), view);
                        LogMessage.debug("Class C: " + Subnets.getSubnetMask());
                        backGroundWork = new BackGroundWork();
                        backGroundWork.execute("CLASS-C");
                        // showResultTable(classC.getListOfSubnets());
                    } else {
                        LogMessage.debug("Invalid Network");
                        SoftkeyBoard.hideKeyboardFrom(getContext(), view);
                        //    hideResultTable();
                    }
                }else {
                    Toast.makeText(getContext(),"Enter Network Between class A - Class C\n" +
                            "First Octate \nClass A: 0 - 127 \nClass B: 128 - 191 \nClass C: 192 - 223",Toast.LENGTH_LONG).show();
                }

            } else {
                LogMessage.debug("Required Field is Empty");
                MyToast.Message("Please Fill the network Address and Host Number");

            }

        }
    }

    private boolean isRequiredFieldEmpty(String s, String s1, String s2, String s3, String s4, String s5) {
        if (s.isEmpty() || s1.isEmpty() || s2.isEmpty() || s3.isEmpty() || s4.isEmpty() || s5.isEmpty()) {
            return true;
        }
        return false;
    }


    class BackGroundWork extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... data) {
            String method = data[0];
            if (method.equals("CLASS-A")) {
                classA.calculateSubnet();
                bundle.putSerializable(SUBNET_LIST, (Serializable) classA.getListOfSubnet());
                resultFragmentSubnetting.setArguments(bundle);
                resultFragmentSubnetting.show(getFragmentManager(), RESULT_FRAGMENT);
            } else if (method.equals("CLASS-B")) {
                classB.calculateSubnet();
                bundle.putSerializable(SUBNET_LIST, (Serializable) classB.getListOfSubnet());
                resultFragmentSubnetting.setArguments(bundle);
                resultFragmentSubnetting.show(getFragmentManager(), RESULT_FRAGMENT);
            } else if (method.equals("CLASS-C")) {
                classC.calculateSubnet();
                bundle.putSerializable(SUBNET_LIST, (Serializable) classC.getListOfSubnets());
                resultFragmentSubnetting.setArguments(bundle);
                resultFragmentSubnetting.show(getFragmentManager(), RESULT_FRAGMENT);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

}
