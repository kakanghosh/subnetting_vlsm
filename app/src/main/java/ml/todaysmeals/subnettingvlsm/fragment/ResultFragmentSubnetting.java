package ml.todaysmeals.subnettingvlsm.fragment;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ml.todaysmeals.subnettingvlsm.R;
import ml.todaysmeals.subnettingvlsm.classes.Subnets;

/**
 * Created by GHOSH on 3/18/2017.
 */

public class ResultFragmentSubnetting extends DialogFragment {
    private AlertDialog.Builder builder;
    private TextView textView;
    private LinearLayout layout;
    private TableLayout resultTable;
    private TableRow row;
    private ScrollView scrollView;
    private boolean resultShown;
    private TableRow tableHeaderRow;
    private TextView subnetMask;
    private LinearLayout subnetMaskLayout;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.result_dialog_subnetting,null);
        subnetMaskLayout = (LinearLayout) view.findViewById(R.id.subnetMaskRow);
        subnetMask = (TextView) view.findViewById(R.id.subnetmasktextView);
        layout = (LinearLayout) view.findViewById(R.id.resultLayout);
        scrollView = (ScrollView) view.findViewById(R.id.resultScrollView);
        tableHeaderRow = (TableRow) view.findViewById(R.id.tableHeaderRow);
        // tableHeader = (TableLayout) view.findViewById(R.id.resultTableHeader);
        resultTable = (TableLayout) view.findViewById(R.id.resultTable);
        builder.setView(view);
        builder.setCancelable(false);
        Dialog dialog = builder.create();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = getArguments();
        ArrayList<Subnets> list = (ArrayList<Subnets>) bundle.getSerializable(Subnetting.SUBNET_LIST);
        tableHeaderRow.setVisibility(View.GONE);
        subnetMaskLayout.setVisibility(View.GONE);
        showResultTable(list);
    }

    private void showResultTable(List<Subnets> listOfSubnets) {

        int leftPadding, topPadding, rigthPadding, bottomPadding;
        int fontSize = 8;
        leftPadding = 15;
        topPadding = 10;
        rigthPadding = 5;
        bottomPadding = 10;

        scrollView.scrollTo(0, 0);
        layout.removeAllViews();
        //resultTable = new TableLayout(getContext());
        // resultTable.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        row = new TableRow(getContext());
        row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
        TextView subnetNumber, networkID, firstHost, lastHost, broadcast;

        resultTable.removeAllViews();
        subnetMaskLayout.setVisibility(View.VISIBLE);
//        resultTable.addView(subnetMaskLayout);
        tableHeaderRow.setVisibility(View.VISIBLE);
        resultTable.addView(tableHeaderRow);
        subnetMask.setText(Subnets.getSubnetMask());
        for (int index = 0; index < listOfSubnets.size(); index++) {
            subnetNumber = new TextView(getContext());
            networkID = new TextView(getContext());
            firstHost = new TextView(getContext());
            lastHost = new TextView(getContext());
            broadcast = new TextView(getContext());
            row = new TableRow(getContext());
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
            TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
            textViewParams.setMargins(5, 5, 5, 5);

            subnetNumber.setLayoutParams(textViewParams);
            networkID.setLayoutParams(textViewParams);
            firstHost.setLayoutParams(textViewParams);
            lastHost.setLayoutParams(textViewParams);
            broadcast.setLayoutParams(textViewParams);

            subnetNumber.setText(Integer.toString(index + 1));
            subnetNumber.setPadding(leftPadding, topPadding, rigthPadding + 70, bottomPadding);
            subnetNumber.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP, fontSize));
            if (index % 2 == 0){
                subnetNumber.setBackgroundColor(getResources().getColor(R.color.color4));
            }else {
                subnetNumber.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }


            networkID.setText(listOfSubnets.get(index).getNetworkID());
            networkID.setPadding(leftPadding, topPadding, rigthPadding, bottomPadding);
            networkID.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP, fontSize));
            if (index % 2 == 0){
                networkID.setBackgroundColor(getResources().getColor(R.color.color4));
            }else {
                networkID.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }

            firstHost.setText(listOfSubnets.get(index).getFirstHost());
            firstHost.setPadding(leftPadding, topPadding, rigthPadding, bottomPadding);
            firstHost.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP, fontSize));
            if (index % 2 == 0){
                firstHost.setBackgroundColor(getResources().getColor(R.color.color4));
            }else {
                firstHost.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }

            lastHost.setText(listOfSubnets.get(index).getLastHost());
            lastHost.setPadding(leftPadding, topPadding, rigthPadding, bottomPadding);
            lastHost.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP, fontSize));
            if (index % 2 == 0){
                lastHost.setBackgroundColor(getResources().getColor(R.color.color4));
            }else {
                lastHost.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }

            broadcast.setText(listOfSubnets.get(index).getBreoadCastAddress());
            broadcast.setPadding(leftPadding, topPadding, rigthPadding, bottomPadding);
            broadcast.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP, fontSize));
            if (index % 2 == 0){
                broadcast.setBackgroundColor(getResources().getColor(R.color.color4));
            }else {
                broadcast.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            }

            row.addView(subnetNumber);
            row.addView(networkID);
            row.addView(firstHost);
            row.addView(lastHost);
            row.addView(broadcast);


            resultTable.addView(row);


        }

        layout.addView(resultTable);

    }

    //This method will hide the result table if any error occured
    private void hideResultTable() {
        layout.removeAllViews();
        //   tableHeader.setVisibility(View.GONE);
        resultShown = false;
    }

    int getPixels(int unit, float size) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return (int) TypedValue.applyDimension(unit, size, metrics);
    }

}
