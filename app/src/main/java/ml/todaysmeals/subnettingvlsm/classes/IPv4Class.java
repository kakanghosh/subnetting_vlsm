package ml.todaysmeals.subnettingvlsm.classes;

import ml.todaysmeals.subnettingvlsm.debug.LogMessage;
import ml.todaysmeals.subnettingvlsm.fragment.Subnetting;

/**
 * Created by GHOSH on 3/17/2017.
 */

public class IPv4Class {
    protected Subnetting subnetting;
    protected int hostNumber;
    protected int firstOctate;
    protected int secondOctate;
    protected int thirdOctate;
    protected int forthOctate;
    protected int cidrNumber;
    protected int defaultNetworkBit;
    protected int defaultHostBit;
    protected int availableHostBit;
    protected int requiredHostBit;
    public IPv4Class(){}
    public IPv4Class(Subnetting subnetting, int hostNumber,int firstOctate, int secondOctate, int thirdOctate, int forthOctate, int cidrNumber) {
        this.subnetting = subnetting;
        this.firstOctate = firstOctate;
        this.hostNumber = hostNumber;
        this.secondOctate = secondOctate;
        this.thirdOctate = thirdOctate;
        this.forthOctate = forthOctate;
        this.cidrNumber = cidrNumber;
        LogMessage.debug(firstOctate+"."+secondOctate+"."+thirdOctate+"."+forthOctate+"/"+cidrNumber);
    }

    public IPv4Class(int firstOctate, int secondOctate, int thirdOctate, int forthOctate, int cidrNumber) {
        this.firstOctate = firstOctate;
        this.secondOctate = secondOctate;
        this.thirdOctate = thirdOctate;
        this.forthOctate = forthOctate;
        this.cidrNumber = cidrNumber;
    }


    public int getRequiredHostBit(int hostNumber, int availableHostBit) {
        int actualHost = hostNumber + 2;
        for (int bit = 2; bit <= availableHostBit; bit++) {
            if (Math.pow(2, bit) >= actualHost) {
                return bit;
            }
        }
        return -1;
    }

    public int getNoOfSubnet(int avaiblebit, int requiredBit) {
        return (int) Math.pow(2, avaiblebit - requiredBit);
    }

    protected String addZeroAfterBinaryString(String binary, int lenth) {
        for (int i = 0; i < lenth; i++) {
            binary = binary + "0";
        }
        return binary;
    }

    protected String addOneAfterBinaryString(String binary, int lenth) {
        for (int i = 0; i < lenth; i++) {
            binary = binary + "1";
        }
        return binary;
    }

    protected String addZeroBeforeBinaryString(String binary, int lenth) {
        for (int i = 0; i < lenth; i++) {
            binary = "0" + binary;
        }
        return binary;
    }

    protected int getDecimalValueFromSubtring(int start, int end, String binaryString){
        return Integer.parseInt(binaryString.substring(start,end),2);
    }

    protected String getSubnetMask(int bit){
        String subnetMaskBit = addOneAfterBinaryString("",bit);
        subnetMaskBit = addZeroAfterBinaryString(subnetMaskBit,32-subnetMaskBit.length());
        return getDecimalValueFromSubtring(0,8,subnetMaskBit)+"."+getDecimalValueFromSubtring(8,16,subnetMaskBit)
                +"."+getDecimalValueFromSubtring(16,24,subnetMaskBit)+"."+getDecimalValueFromSubtring(24,32,subnetMaskBit);
    }

    public int getCidrNumber() {
        return cidrNumber;
    }

    public int getForthOctate() {
        return forthOctate;
    }

    public int getThirdOctate() {
        return thirdOctate;
    }

    public int getSecondOctate() {
        return secondOctate;
    }

    public int getFirstOctate() {
        return firstOctate;
    }
}
