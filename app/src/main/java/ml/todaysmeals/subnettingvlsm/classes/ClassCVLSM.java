package ml.todaysmeals.subnettingvlsm.classes;

import java.util.ArrayList;
import java.util.List;

import ml.todaysmeals.subnettingvlsm.datastructure.Sorting;
import ml.todaysmeals.subnettingvlsm.debug.LogMessage;
import ml.todaysmeals.subnettingvlsm.fragment.VLSM;

/**
 * Created by GHOSH on 3/19/2017.
 */

public class ClassCVLSM extends IPv4Class {
    private VLSM vlsm;
    private List<SubnetBlock> listOfVLSM;
    private String bit8;
    private boolean vlsmIsNotPossible;

    public ClassCVLSM(VLSM vlsm, List<Integer> listOfHost, int firstOctate, int secondOctate, int thirdOctate, int forthOctate, int cidrNumber) {
        super(firstOctate, secondOctate, thirdOctate, forthOctate, cidrNumber);
        this.vlsm = vlsm;
        this.listOfVLSM = new ArrayList<>();
        this.defaultNetworkBit = 24;
        this.defaultHostBit = 8;
        for (int i = 0; i < listOfHost.size(); i++) {
            SubnetBlock subnetBlock = new SubnetBlock();
            subnetBlock.hostNumber = listOfHost.get(i);
            subnetBlock.serialNumber = (i + 1);
            this.listOfVLSM.add(subnetBlock);
        }
        showListOfHost(listOfVLSM);
        Sorting.sortByHostNumber(listOfVLSM);
        showListOfHost(listOfVLSM);
    }

    public void calculteVLSM() {
        String networkAddress, broadcastAddress;
        String firstHost, lastHost;
        String subnetBitString;
        int forthOctatValue;
        int subnetBit;
        for (int index = 0; index < this.listOfVLSM.size(); index++) {
            if (validateNetworkID(this.forthOctate, this.cidrNumber, this.defaultNetworkBit, this.listOfVLSM.get(index).hostNumber)) {
               // LogMessage.debug("Valid Network Address");
                availableHostBit = 32 - cidrNumber;
                requiredHostBit = getRequiredHostBit(this.listOfVLSM.get(index).hostNumber, availableHostBit);
                subnetBit = availableHostBit - requiredHostBit;
                String preValue = bit8.substring(0, cidrNumber - defaultNetworkBit);
                if (getNoOfSubnet(availableHostBit, requiredHostBit) > 1) {
                    subnetBitString = Integer.toBinaryString(0);
                    subnetBitString = addZeroBeforeBinaryString(subnetBitString, subnetBit - subnetBitString.length());
                    forthOctatValue = getDecimalValueFromSubtring(0, 8, preValue + addZeroAfterBinaryString(subnetBitString, 32 - (cidrNumber + subnetBit)));
                    networkAddress = firstOctate + "." + secondOctate + "." + thirdOctate + "." + forthOctatValue;
                    firstHost = firstOctate + "." + secondOctate + "." + thirdOctate + "." + (forthOctatValue + 1);
                    forthOctatValue = getDecimalValueFromSubtring(0, 8, preValue + addOneAfterBinaryString(subnetBitString, 32 - (cidrNumber + subnetBit)));
                    broadcastAddress = firstOctate + "." + secondOctate + "." + thirdOctate + "." + forthOctatValue;
                    lastHost = firstOctate + "." + secondOctate + "." + thirdOctate + "." + (forthOctatValue - 1);
                  //  LogMessage.debug("IF " + networkAddress + "  -  " + firstHost + " - " + lastHost + "  - " + broadcastAddress + " / " + (cidrNumber + subnetBit));
                    Subnets subnet = new Subnets(networkAddress, broadcastAddress, firstHost, lastHost, cidrNumber + subnetBit);
                    //listOfVLSM.add(subnet);
                    listOfVLSM.get(index).subnets = subnet;
                    //Lets do it
                    subnetBitString = Integer.toBinaryString(1);
                    subnetBitString = addZeroBeforeBinaryString(subnetBitString, subnetBit - subnetBitString.length());
                    forthOctatValue = getDecimalValueFromSubtring(0, 8, preValue + addZeroAfterBinaryString(subnetBitString, 32 - (cidrNumber + subnetBit)));
                    forthOctate = forthOctatValue;
                    cidrNumber = cidrNumber + subnetBit;
                   // networkAddress = firstOctate + "." + secondOctate + "." + thirdOctate + "." + forthOctatValue;
                    //      LogMessage.debug("IF Next Network Address: " + networkAddress + "/ " + cidrNumber);
                } else if (getNoOfSubnet(availableHostBit, requiredHostBit) == 1) {
                  //  LogMessage.debug("Pre Value: " + preValue);
               //     LogMessage.debug("Subnet Bit: " + subnetBit);
                    forthOctatValue = getDecimalValueFromSubtring(0, 8, addZeroAfterBinaryString(preValue, 32 - (cidrNumber + subnetBit)));
                    networkAddress = firstOctate + "." + secondOctate + "." + thirdOctate + "." + forthOctatValue;
                    firstHost = firstOctate + "." + secondOctate + "." + thirdOctate + "." + (forthOctatValue + 1);
                    forthOctatValue = getDecimalValueFromSubtring(0, 8, addOneAfterBinaryString(preValue, 32 - (cidrNumber + subnetBit)));
                    broadcastAddress = firstOctate + "." + secondOctate + "." + thirdOctate + "." + forthOctatValue;
                    lastHost = firstOctate + "." + secondOctate + "." + thirdOctate + "." + (forthOctatValue - 1);
                 //   LogMessage.debug("else if: " + networkAddress + "  -  " + firstHost + " - " + lastHost + " - " + broadcastAddress + " / " + (cidrNumber + subnetBit));
                    Subnets subnet = new Subnets(networkAddress, broadcastAddress, firstHost, lastHost, cidrNumber);
                    //listOfVLSM.add(subnet);
                    listOfVLSM.get(index).subnets = subnet;

                    forthOctate = forthOctatValue + 1;
                    cidrNumber = cidrNumber + subnetBit;
                    //networkAddress = firstOctate + "." + secondOctate + "." + thirdOctate + "." + forthOctate;
                    //      LogMessage.debug("ELSE IF Next Network Address: " + networkAddress + "/ " + cidrNumber);
                } else {
                    LogMessage.debug(">>>>>>>>>><<<<<<<<<<<<<<<<<<<<");
                }
            } else {
                this.vlsmIsNotPossible = true;
                LogMessage.debug("VLSM can not possible");
                break;
            }
        }
        //Sorting.sortBySerialNumber(listOfVLSM);
    }


    public boolean validateNetworkID(int forthOctate, int cidr, int defultNetworkBit, int host) {
        if (cidr - defultNetworkBit < 0) {
            LogMessage.debug("Invalid Network Address");
            return false;
        }
        int availableHostBit = 32 - cidr;
        int requiredHostBit = getRequiredHostBit(host, availableHostBit);
        if (requiredHostBit == -1) {
            LogMessage.debug("Excess Of Host");
            return false;
        }

        String forthOctateBinaryString = Integer.toBinaryString(forthOctate);
        LogMessage.debug("Forth Octate Binary String: " + forthOctateBinaryString);
        if (forthOctateBinaryString.length() > 8) {
            return false;
        }
        forthOctateBinaryString = addZeroBeforeBinaryString(forthOctateBinaryString, 8 - forthOctateBinaryString.length());
        bit8 = forthOctateBinaryString;
        forthOctateBinaryString = forthOctateBinaryString.substring(cidr - defultNetworkBit, forthOctateBinaryString.length());
        if (Integer.parseInt(forthOctateBinaryString, 2) == 0) {
            return true;
        }
        LogMessage.debug("Invalid Network Address");
        return false;
    }

    private void showListOfHost(List<SubnetBlock> listOfHostNumber) {
        for (int i = 0; i < listOfHostNumber.size(); i++) {
            LogMessage.debug("Item " + (i + 1) + " : Host: " + listOfHostNumber.get(i).hostNumber + " Serial: " + listOfHostNumber.get(i).serialNumber);
        }
        LogMessage.debug(">>>>>>>>>>>><<<<<<<<<<<<<");
    }

    public void showVLSM() {
        List<SubnetBlock> listOfHostNumber = listOfVLSM;
        //Sorting.sortBySerialNumber(listOfHostNumber);
        if (!vlsmIsNotPossible) {
            for (int i = 0; i < listOfHostNumber.size(); i++) {
                LogMessage.debug("Item " + (i + 1) + " : Host: " + listOfHostNumber.get(i).hostNumber + " Serial: " + listOfHostNumber.get(i).serialNumber);
                LogMessage.debug(listOfHostNumber.get(i).subnets.getNetworkID() + " - " + listOfHostNumber.get(i).subnets.getFirstHost()
                        + " - " + listOfHostNumber.get(i).subnets.getLastHost() + " - " + listOfHostNumber.get(i).subnets.getBreoadCastAddress() + " / " +
                        listOfHostNumber.get(i).subnets.getCidrVALUE());
            }
            LogMessage.debug(">>>>>>>>>>>><<<<<<<<<<<<<");
        }
    }

    public List<SubnetBlock> getListOfVLSM(){
        if (vlsmIsNotPossible){
            return null;
        }
        return this.listOfVLSM;
    }

}
