package ml.todaysmeals.subnettingvlsm.classes;

import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ml.todaysmeals.subnettingvlsm.debug.LogMessage;
import ml.todaysmeals.subnettingvlsm.fragment.Subnetting;

/**
 * Created by GHOSH on 3/17/2017.
 */

public class ClassB extends IPv4Class {
    private int defaultNetworkBit;
    private int defaultHostBit;
    private int availableHostBit;
    private int requiredHostBit;
    private String thirdOctateBinaryString;
    private String forthOctateBinaryString;
    private String bit16;
    private String totalString;
    private List<Subnets> listOfSubnet;

    public ClassB(Subnetting subnetting, int hostNumber, int firstOctate, int second, int third, int forth, int cidrNumber) {
        super(subnetting, hostNumber, firstOctate, second, third, forth, cidrNumber);
        this.listOfSubnet = new ArrayList<>();
        this.defaultNetworkBit = 16;
        this.defaultHostBit = 16;
        this.availableHostBit = 32 - cidrNumber;
        requiredHostBit = getRequiredHostBit(hostNumber, availableHostBit);
        LogMessage.debug("Available Host bit: " + availableHostBit);
        LogMessage.debug("Required Host bit: " + requiredHostBit);
    }

    public void calculateSubnet() {
        LogMessage.debug("Total String: " + bit16);
        String last16BitAfterSubnetting1 = "";
        String last16BitAfterSubnetting2 = "";
        String networkAddress = "";
        String firstHostIP = "";
        String lastHostIP = "";
        String broadcastAddress = "";
        String subnetbitString = ""; // The combination value of subnet bit
        String preValueOfSubnetbitString = ""; //The value of previous bit 172.16.(0001)|0000|.000000000, bracket value as pre value
        preValueOfSubnetbitString = bit16.substring(0, cidrNumber - defaultNetworkBit);
        LogMessage.debug("Pre value: " + preValueOfSubnetbitString);
        //number of subnet will be 2 ^ (available host bit - required host bit)
        int subnetBit = availableHostBit - requiredHostBit;
        for (int i = 0; i < Math.pow(2, subnetBit); i++) {
            subnetbitString = Integer.toBinaryString(i);
            if (subnetbitString.length() < subnetBit) {
                //TODO
                /*If subnetbitString is than the subnet bit length
                    then these number of zero's will be added before the subnet bit string
                    Example: subnetbit = 3
                    first subnet bit String =  0
                    so 2 zero will added before subnetbitString
                */
                subnetbitString = addZeroBeforeBinaryString(subnetbitString, subnetBit - subnetbitString.length());
                // LogMessage.debug("Subnetting String: "+subnetbitString);
            }
            if (subnetBit == 0)
                subnetbitString = "";

            last16BitAfterSubnetting1 = preValueOfSubnetbitString + addZeroAfterBinaryString(subnetbitString, 32 - (cidrNumber + subnetBit));
            last16BitAfterSubnetting2 = preValueOfSubnetbitString + addOneAfterBinaryString(subnetbitString, 32 - (cidrNumber + subnetBit));
            networkAddress = firstOctate + "." + secondOctate + "." + getDecimalValueFromSubtring(0, 8, last16BitAfterSubnetting1) + "." + getDecimalValueFromSubtring(8, 16, last16BitAfterSubnetting1);
            broadcastAddress = firstOctate + "." + secondOctate + "." + getDecimalValueFromSubtring(0, 8, last16BitAfterSubnetting2) + "." + getDecimalValueFromSubtring(8, 16, last16BitAfterSubnetting2);
            firstHostIP = firstOctate + "." + secondOctate + "." + getDecimalValueFromSubtring(0, 8, last16BitAfterSubnetting1) + "." + (getDecimalValueFromSubtring(8, 16, last16BitAfterSubnetting1) + 1);
            lastHostIP = firstOctate + "." + secondOctate + "." + getDecimalValueFromSubtring(0, 8, last16BitAfterSubnetting2) + "." + (getDecimalValueFromSubtring(8, 16, last16BitAfterSubnetting2) - 1);

            listOfSubnet.add(new Subnets(networkAddress, broadcastAddress, firstHostIP, lastHostIP));

            LogMessage.debug(networkAddress + " - " + firstHostIP + " - " + lastHostIP + " - " + broadcastAddress);

        }
        Subnets.setSubnetMask(getSubnetMask(cidrNumber + subnetBit));
        LogMessage.debug("Subnet Mask: " + Subnets.getSubnetMask());
    }

    public boolean validateNetworkAddress() {
        if (cidrNumber - defaultNetworkBit >= 0){
            thirdOctateBinaryString = Integer.toBinaryString(thirdOctate); // Binary String of Third Octate
            forthOctateBinaryString = Integer.toBinaryString(forthOctate); // Binary String of Forth Octate
            thirdOctateBinaryString = addZeroBeforeBinaryString(thirdOctateBinaryString, 8 - thirdOctateBinaryString.length());
            forthOctateBinaryString = addZeroBeforeBinaryString(forthOctateBinaryString, 8 - forthOctateBinaryString.length());
            // Concation of two Binary String
            totalString = thirdOctateBinaryString + forthOctateBinaryString;
            bit16 = totalString;
            // Getting the substring from CIDR value to last
            //  (CIDR - defaultNetworkBit) to last
            totalString = totalString.substring((cidrNumber - defaultNetworkBit), totalString.length());
            LogMessage.debug(totalString);
            LogMessage.debug("" + Integer.parseInt(totalString, 2));
            if (Integer.parseInt(totalString, 2) == 0) {
                if (requiredHostBit <= availableHostBit && requiredHostBit != -1) {
                    return true;
                } else {
                    Toast.makeText(subnetting.getContext(), "Excess Of Host", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        Toast.makeText(subnetting.getContext(), "Invalid Network", Toast.LENGTH_SHORT).show();
        return false;
    }

    public List<Subnets> getListOfSubnet() {
        return this.listOfSubnet;
    }
}
