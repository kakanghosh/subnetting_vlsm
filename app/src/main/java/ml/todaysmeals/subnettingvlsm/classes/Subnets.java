package ml.todaysmeals.subnettingvlsm.classes;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by GHOSH on 3/15/2017.
 */

public class Subnets implements Serializable {
    private String networkID;
    private String breoadCastAddress;
    private String firstHost;
    private String lastHost;
    private int cidrVALUE;
    public static String subnetMask;

    public Subnets(){}

    public Subnets(String networkID, String breoadCastAddress, String firstHost, String lastHost) {
        this.networkID = networkID;
        this.breoadCastAddress = breoadCastAddress;
        this.firstHost = firstHost;
        this.lastHost = lastHost;
    }

    public Subnets(String networkID, String breoadCastAddress, String firstHost, String lastHost, int cidrVALUE) {
        this.networkID = networkID;
        this.breoadCastAddress = breoadCastAddress;
        this.firstHost = firstHost;
        this.lastHost = lastHost;
        this.cidrVALUE = cidrVALUE;
    }

    public int getCidrVALUE() {
        return cidrVALUE;
    }

    public void setCidrVALUE(int cidrVALUE) {
        this.cidrVALUE = cidrVALUE;
    }

    public String getNetworkID() {
        return networkID;
    }

    public void setNetworkID(String networkID) {
        this.networkID = networkID;
    }

    public String getBreoadCastAddress() {
        return breoadCastAddress;
    }

    public void setBreoadCastAddress(String breoadCastAddress) {
        this.breoadCastAddress = breoadCastAddress;
    }

    public String getFirstHost() {
        return firstHost;
    }

    public void setFirstHost(String firstHost) {
        this.firstHost = firstHost;
    }

    public String getLastHost() {
        return lastHost;
    }

    public void setLastHost(String lastHost) {
        this.lastHost = lastHost;
    }

    public static  String  getSubnetMask() {
        return subnetMask;
    }

    public static void setSubnetMask(String sub) {
       subnetMask = sub;
    }


}
