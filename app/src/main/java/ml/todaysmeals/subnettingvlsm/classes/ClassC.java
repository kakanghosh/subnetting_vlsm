package ml.todaysmeals.subnettingvlsm.classes;

import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ml.todaysmeals.subnettingvlsm.debug.LogMessage;
import ml.todaysmeals.subnettingvlsm.debug.MyToast;
import ml.todaysmeals.subnettingvlsm.fragment.Subnetting;

import static ml.todaysmeals.subnettingvlsm.classes.Subnets.getSubnetMask;

/**
 * Created by GHOSH on 3/15/2017.
 */

public class ClassC extends IPv4Class {
    private String bit8;
    private List<Subnets> listOfSubnets;

    public ClassC(Subnetting subnetting, int hostNumber, int firstOctate, int secondOctate, int thirdOctate, int forthOctate, int cidrNumber) {
        super(subnetting, hostNumber, firstOctate, secondOctate, thirdOctate, forthOctate, cidrNumber);
        this.listOfSubnets = new ArrayList<>();
        this.defaultNetworkBit = 24;
        this.defaultHostBit = 8;
        this.availableHostBit = 32 - cidrNumber;
        this.requiredHostBit = getRequiredHostBit(hostNumber, availableHostBit);
        LogMessage.debug("Available Host bit: " + availableHostBit);
        LogMessage.debug("Required Host bit: " + requiredHostBit);
    }

    public ClassC(int firstOctate, int secondOctate, int thirdOctate, int forthOctate, int cidrNumber) {
       super(firstOctate,secondOctate,thirdOctate,forthOctate,cidrNumber);
        this.defaultNetworkBit = 24;
        this.defaultHostBit = 8;
        this.availableHostBit = 32 - cidrNumber;
        LogMessage.debug("Available Host bit: " + availableHostBit);
    }


    public void calculateSubnet() {
        String networkAddress, broadcastAddress;
        String firstHost, lastHost;
        int subnetBit = (availableHostBit - requiredHostBit);
        String preValue = bit8.substring(0, cidrNumber - defaultNetworkBit);
        for (int i = 0; i < Math.pow(2, subnetBit); i++) {
            String subnetBitString = Integer.toBinaryString(i);
            subnetBitString = addZeroBeforeBinaryString(subnetBitString, subnetBit - subnetBitString.length());
            if (subnetBit == 0)
                subnetBitString = "";
            networkAddress = firstOctate + "." + secondOctate + "." + thirdOctate + "." + getDecimalValueFromSubtring(0, 8, preValue + addZeroAfterBinaryString(subnetBitString, 32 - (cidrNumber + subnetBit)));
            broadcastAddress = firstOctate + "." + secondOctate + "." + thirdOctate + "." + getDecimalValueFromSubtring(0, 8, preValue + addOneAfterBinaryString(subnetBitString, 32 - (cidrNumber + subnetBit)));
            firstHost = firstOctate + "." + secondOctate + "." + thirdOctate + "." + (getDecimalValueFromSubtring(0, 8, preValue + addZeroAfterBinaryString(subnetBitString, 32 - (cidrNumber + subnetBit))) + 1);
            lastHost = firstOctate + "." + secondOctate + "." + thirdOctate + "." + (getDecimalValueFromSubtring(0, 8, preValue + addOneAfterBinaryString(subnetBitString, 32 - (cidrNumber + subnetBit))) - 1);
            listOfSubnets.add(new Subnets(networkAddress, broadcastAddress, firstHost, lastHost));
            LogMessage.debug(networkAddress + " - " + firstHost + " - " + lastHost + " - " + broadcastAddress);

        }

        Subnets.setSubnetMask(getSubnetMask(cidrNumber + subnetBit));
        LogMessage.debug("Subnet Mask: " + Subnets.getSubnetMask());
    }

    public List<Subnets> getListOfSubnets() {
        return listOfSubnets;
    }

    public boolean validateNetworkID() {

        if (cidrNumber - defaultNetworkBit >= 0) {
            String forthOctateBinaryString = Integer.toBinaryString(forthOctate);
            forthOctateBinaryString = addZeroBeforeBinaryString(forthOctateBinaryString, 8 - forthOctateBinaryString.length());
            bit8 = forthOctateBinaryString;
            forthOctateBinaryString = forthOctateBinaryString.substring(cidrNumber - defaultNetworkBit, forthOctateBinaryString.length());
            if (Integer.parseInt(forthOctateBinaryString, 2) == 0) {
                if (requiredHostBit <= availableHostBit && requiredHostBit != -1) {
                    return true;
                } else {
                    LogMessage.debug("Excess of Host");
                    MyToast.Message("Excess of Host");
                    return false;
                }
            }
        }
        MyToast.Message("Invalid Network");
        return false;
    }


    public boolean validateNetworkID(int forthOctateNumber,int cidr,int defaultNetBit,int required,int available) {

        if (cidr - defaultNetBit >= 0) {
            String forthOctateBinaryString = Integer.toBinaryString(forthOctateNumber);
            forthOctateBinaryString = addZeroBeforeBinaryString(forthOctateBinaryString, 8 - forthOctateBinaryString.length());
            forthOctateBinaryString = forthOctateBinaryString.substring(cidr - defaultNetBit, forthOctateBinaryString.length());
            if (Integer.parseInt(forthOctateBinaryString, 2) == 0) {
                if (required <= available && required != -1) {
                    return true;
                } else {
                    LogMessage.debug("Excess of Host");
                    MyToast.Message("Excess of Host");
                    return false;
                }
            }
        }
        MyToast.Message("Invalid Network");
        return false;
    }


    public int getDefaultNetworkBit() {
        return defaultNetworkBit;
    }

    public int getDefaultHostBit() {
        return defaultHostBit;
    }

    public int getAvailableHostBit() {
        return availableHostBit;
    }

    public int getRequiredHostBit() {
        return requiredHostBit;
    }


}
