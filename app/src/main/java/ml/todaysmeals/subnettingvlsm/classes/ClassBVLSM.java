package ml.todaysmeals.subnettingvlsm.classes;

import java.util.ArrayList;
import java.util.List;

import ml.todaysmeals.subnettingvlsm.datastructure.Sorting;
import ml.todaysmeals.subnettingvlsm.debug.LogMessage;
import ml.todaysmeals.subnettingvlsm.fragment.VLSM;

/**
 * Created by GHOSH on 3/22/2017.
 */

public class ClassBVLSM extends IPv4Class {
    private VLSM vlsm;
    private List<SubnetBlock> listOfVLSM;
    private String bit16;
    private boolean vlsmIsNotPossible;

    public ClassBVLSM(VLSM vlsm, List<Integer> listOfHost, int firstOctate, int secondOctate, int thirdOctate, int forthOctate, int cidrNumber) {
        super(firstOctate, secondOctate, thirdOctate, forthOctate, cidrNumber);
        this.vlsm = vlsm;
        this.listOfVLSM = new ArrayList<>();
        this.defaultNetworkBit = 16;
        this.defaultHostBit = 16;
        for (int i = 0; i < listOfHost.size(); i++) {
            SubnetBlock subnetBlock = new SubnetBlock();
            subnetBlock.hostNumber = listOfHost.get(i);
            subnetBlock.serialNumber = (i + 1);
            this.listOfVLSM.add(subnetBlock);
        }
        // showListOfHost(listOfVLSM);
        Sorting.sortByHostNumber(listOfVLSM);
        //showListOfHost(listOfVLSM);
    }

    public void calculateVLSM() {
        String networkAddress, broadcastAddress;
        String firstHost, lastHost;
        String subnetBitString;
        String last16BitAfterSubnetting1 = "";
        String last16BitAfterSubnetting2 = "";
        int thirdOctateValue, forthOctateValue;
        int subnetBit;
        for (int index = 0; index < this.listOfVLSM.size(); index++) {
            if (validateNetworkID(thirdOctate, forthOctate, cidrNumber, defaultNetworkBit, listOfVLSM.get(index).hostNumber)) {
                availableHostBit = 32 - cidrNumber;
                requiredHostBit = getRequiredHostBit(listOfVLSM.get(index).hostNumber, availableHostBit);
                subnetBit = availableHostBit - requiredHostBit;
                String preValue = bit16.substring(0, cidrNumber - defaultNetworkBit);
                if (getNoOfSubnet(availableHostBit, requiredHostBit) > 1) {
                    subnetBitString = Integer.toBinaryString(0);
                    subnetBitString = addZeroBeforeBinaryString(subnetBitString, subnetBit - subnetBitString.length());
                    last16BitAfterSubnetting1 = preValue + addZeroAfterBinaryString(subnetBitString, 32 - (cidrNumber + subnetBit));
                    last16BitAfterSubnetting2 = preValue + addOneAfterBinaryString(subnetBitString, 32 - (cidrNumber + subnetBit));
                    thirdOctateValue = getDecimalValueFromSubtring(0, 8, last16BitAfterSubnetting1);
                    forthOctateValue = getDecimalValueFromSubtring(8, 16, last16BitAfterSubnetting1);
                    networkAddress = firstOctate + "." + secondOctate + "." + thirdOctateValue + "." + forthOctateValue;
                    firstHost = firstOctate + "." + secondOctate + "." + thirdOctateValue + "." + (forthOctateValue + 1);
                    thirdOctateValue = getDecimalValueFromSubtring(0, 8, last16BitAfterSubnetting2);
                    forthOctateValue = getDecimalValueFromSubtring(8, 16, last16BitAfterSubnetting2);
                    broadcastAddress = firstOctate + "." + secondOctate + "." + thirdOctateValue + "." + forthOctateValue;
                    lastHost = firstOctate + "." + secondOctate + "." + thirdOctateValue + "." + (forthOctateValue - 1);
                    LogMessage.debug("Network Range: " + networkAddress + " - " + broadcastAddress + " / " + (cidrNumber + subnetBit));
                    Subnets subnet = new Subnets(networkAddress, broadcastAddress, firstHost, lastHost, cidrNumber + subnetBit);
                    //listOfVLSM.add(subnet);
                    listOfVLSM.get(index).subnets = subnet;

                    subnetBitString = Integer.toBinaryString(1);
                    subnetBitString = addZeroBeforeBinaryString(subnetBitString, subnetBit - subnetBitString.length());
                    last16BitAfterSubnetting1 = preValue + addZeroAfterBinaryString(subnetBitString, 32 - (cidrNumber + subnetBit));
                    thirdOctateValue = getDecimalValueFromSubtring(0, 8, last16BitAfterSubnetting1);
                    forthOctateValue = getDecimalValueFromSubtring(8, 16, last16BitAfterSubnetting1);
                    thirdOctate = thirdOctateValue;
                    forthOctate = forthOctateValue;
                    cidrNumber = cidrNumber + subnetBit;

                    networkAddress = firstOctate + "." + secondOctate + "." + thirdOctateValue + "." + forthOctateValue;
                    LogMessage.debug("IF Next Network Address: " + networkAddress + "/ " + cidrNumber);
                }else if (getNoOfSubnet(availableHostBit,requiredHostBit) == 1){
                    last16BitAfterSubnetting1 = addZeroAfterBinaryString(preValue, 32 - (cidrNumber + subnetBit));
                    last16BitAfterSubnetting2 = addOneAfterBinaryString(preValue, 32 - (cidrNumber + subnetBit));
                    thirdOctateValue = getDecimalValueFromSubtring(0, 8, last16BitAfterSubnetting1);
                    forthOctateValue = getDecimalValueFromSubtring(8, 16, last16BitAfterSubnetting1);
                    networkAddress = firstOctate + "." + secondOctate + "." + thirdOctateValue + "." + forthOctateValue;
                    firstHost = firstOctate + "." + secondOctate + "." + thirdOctateValue + "." + (forthOctateValue + 1);
                    thirdOctateValue = getDecimalValueFromSubtring(0, 8, last16BitAfterSubnetting2);
                    forthOctateValue = getDecimalValueFromSubtring(8, 16, last16BitAfterSubnetting2);
                    broadcastAddress = firstOctate + "." + secondOctate + "." + thirdOctateValue + "." + forthOctateValue;
                    lastHost = firstOctate + "." + secondOctate + "." + thirdOctateValue + "." + (forthOctateValue - 1);
                    LogMessage.debug("Network Range: " + networkAddress + " - " + broadcastAddress + " / " + (cidrNumber + subnetBit));
                    Subnets subnet = new Subnets(networkAddress, broadcastAddress, firstHost, lastHost, cidrNumber + subnetBit);
                    listOfVLSM.get(index).subnets = subnet;


                    if (forthOctateValue + 1 > 255){
                        forthOctate = 0;
                        thirdOctate = thirdOctateValue + 1;
                    }
                    cidrNumber = cidrNumber + subnetBit;
                    networkAddress = firstOctate + "." + secondOctate + "." + thirdOctate + "." + forthOctate;
                    LogMessage.debug("ELSE IF Next Network Address: " + networkAddress + "/ " + cidrNumber);
                }
            }else {
                vlsmIsNotPossible = true;
                listOfVLSM.clear();
                break;
            }
        }
    }

    public List<SubnetBlock> getListOfVLSM(){
        if (vlsmIsNotPossible){
            return null;
        }
        return listOfVLSM;
    }

    public boolean validateNetworkID(int thirdOctate, int forthOctate, int cidr, int defultNetworkBit, int host) {
        if (cidr - defultNetworkBit < 0) {
            LogMessage.debug("Invalid Network Address");
            return false;
        }

        int availableHostBit = 32 - cidr;
        int requiredHostBit = getRequiredHostBit(host, availableHostBit);
        if (requiredHostBit == -1) {
            LogMessage.debug("Excess Of Host");
            return false;
        }
        String thirdOctateBinaryString = Integer.toBinaryString(thirdOctate);
        String forthOctateBinaryString = Integer.toBinaryString(forthOctate);
        if (thirdOctateBinaryString.length() > 8) {
            return false;
        }

        thirdOctateBinaryString = addZeroBeforeBinaryString(thirdOctateBinaryString, 8 - thirdOctateBinaryString.length());
        forthOctateBinaryString = addZeroBeforeBinaryString(forthOctateBinaryString, 8 - forthOctateBinaryString.length());
        String totalBinaryString = thirdOctateBinaryString + forthOctateBinaryString;
        bit16 = totalBinaryString;
        totalBinaryString = totalBinaryString.substring((cidrNumber - defaultNetworkBit), totalBinaryString.length());

        if (Integer.parseInt(totalBinaryString, 2) == 0) {
            return true;
        }
        LogMessage.debug("Invalid Network Address");
        return false;
    }
}
