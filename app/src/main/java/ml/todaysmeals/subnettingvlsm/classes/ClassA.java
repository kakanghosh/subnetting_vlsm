package ml.todaysmeals.subnettingvlsm.classes;

import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ml.todaysmeals.subnettingvlsm.debug.LogMessage;
import ml.todaysmeals.subnettingvlsm.fragment.Subnetting;

/**
 * Created by GHOSH on 3/18/2017.
 */

public class ClassA extends IPv4Class {
    protected int defaultNetworkBit;
    protected int defaultHostBit;
    protected int availableHostBit;
    protected int requiredHostBit;
    private String bit24;
    private List<Subnets> listOfSubnet;

    public ClassA(Subnetting subnetting, int hostNumber, int firstOctate, int secondOctate, int thirdOctate, int forthOctate, int cidrNumber) {
        super(subnetting, hostNumber, firstOctate, secondOctate, thirdOctate, forthOctate, cidrNumber);
        this.listOfSubnet = new ArrayList<>();
        this.defaultNetworkBit = 8;
        this.defaultHostBit = 24;
        this.availableHostBit = 32 - cidrNumber;
        this.requiredHostBit = getRequiredHostBit(hostNumber, availableHostBit);
        LogMessage.debug("Available Host bit: " + availableHostBit);
        LogMessage.debug("Required Host bit: " + requiredHostBit);
    }

    public void calculateSubnet(){
        String networkAddress = "";
        String broadcasrAddress = "";
        String firstHostIp = "";
        String lastHostIP = "";
        String preValueOfSubnetBitString = bit24.substring(0,cidrNumber-defaultNetworkBit);
        String last24Bit1,last24Bit2;
        int subnetBit = availableHostBit - requiredHostBit;
        String subnetBitBinaryString = "";
        for (int i = 0; i < Math.pow(2,subnetBit); i++){
            subnetBitBinaryString = Integer.toBinaryString(i);
            if (subnetBitBinaryString.length() < subnetBit){
                subnetBitBinaryString = addZeroBeforeBinaryString(subnetBitBinaryString,subnetBit-subnetBitBinaryString.length());
            }
            if (subnetBit == 0)
                subnetBitBinaryString = "";
            last24Bit1 = preValueOfSubnetBitString + addZeroAfterBinaryString(subnetBitBinaryString,32-(cidrNumber+subnetBit));
            last24Bit2 = preValueOfSubnetBitString + addOneAfterBinaryString(subnetBitBinaryString,32-(cidrNumber+subnetBit));
            networkAddress = firstOctate + "." + getDecimalValueFromSubtring(0,8,last24Bit1) + "."+getDecimalValueFromSubtring(8,16,last24Bit1)+
                    "."+getDecimalValueFromSubtring(16,24,last24Bit1);
            broadcasrAddress = firstOctate + "." + getDecimalValueFromSubtring(0,8,last24Bit2) + "."+
                    getDecimalValueFromSubtring(8,16,last24Bit2)+"."+getDecimalValueFromSubtring(16,24,last24Bit2);
            firstHostIp = firstOctate + "." + getDecimalValueFromSubtring(0,8,last24Bit1) + "."+getDecimalValueFromSubtring(8,16,last24Bit1)+
                    "."+(getDecimalValueFromSubtring(16,24,last24Bit1)+1);
            lastHostIP = firstOctate + "." + getDecimalValueFromSubtring(0,8,last24Bit2) + "."+
                    getDecimalValueFromSubtring(8,16,last24Bit2)+"."+(getDecimalValueFromSubtring(16,24,last24Bit2)-1);
            listOfSubnet.add(new Subnets(networkAddress,broadcasrAddress,firstHostIp,lastHostIP));

            LogMessage.debug(networkAddress + " - "+ firstHostIp+" - "+lastHostIP+" - "+broadcasrAddress);

        }
        Subnets.setSubnetMask(getSubnetMask(cidrNumber+subnetBit));
        LogMessage.debug("Subnet Mask: "+Subnets.getSubnetMask());
    }

    public boolean validateNetworkAddress() {
        if (cidrNumber - defaultNetworkBit >= 0){
            String secondOctateBinaryString = Integer.toBinaryString(secondOctate);
            String thirdOctateBinaryString = Integer.toBinaryString(thirdOctate);
            String forthOctateBinaryString = Integer.toBinaryString(forthOctate);
            String totalString;
            secondOctateBinaryString = addZeroBeforeBinaryString(secondOctateBinaryString, 8 - secondOctateBinaryString.length());
            thirdOctateBinaryString = addZeroBeforeBinaryString(thirdOctateBinaryString, 8 - thirdOctateBinaryString.length());
            forthOctateBinaryString = addZeroBeforeBinaryString(forthOctateBinaryString, 8 - forthOctateBinaryString.length());
            totalString = secondOctateBinaryString + thirdOctateBinaryString + forthOctateBinaryString;
            bit24 = totalString;
            totalString = totalString.substring(cidrNumber - defaultNetworkBit,totalString.length());
            if ( Integer.parseInt(totalString,2) == 0){
                if (requiredHostBit <= availableHostBit){
                    return true;
                }else {
                    Toast.makeText(subnetting.getContext(),"Excess of Host",Toast.LENGTH_SHORT).show();
                    return  false;
                }
            }
        }
        Toast.makeText(subnetting.getContext(),"Invalid Network Address",Toast.LENGTH_SHORT).show();
        return false;
    }

    public List<Subnets> getListOfSubnet(){
        return listOfSubnet;
    }
}
