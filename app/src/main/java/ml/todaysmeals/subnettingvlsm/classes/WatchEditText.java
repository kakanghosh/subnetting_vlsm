package ml.todaysmeals.subnettingvlsm.classes;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import ml.todaysmeals.subnettingvlsm.debug.LogMessage;

/**
 * Created by GHOSH on 3/18/2017.
 */

public class WatchEditText {

    public static class OctateText implements TextWatcher{
        private int lowLimit;
        private int highLimit;
        private EditText editText;
        public OctateText(int lowLimit, int highLimit, EditText editText){
            this.lowLimit = lowLimit;
            this.highLimit = highLimit;
            this.editText = editText;
        }
        @Override
        public void beforeTextChanged(CharSequence value, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence value, int start, int before, int count) {
            if (value.length() > 0){
                int actualValue = Integer.parseInt(value.toString());
                LogMessage.debug("Actual Value: "+actualValue);
                if (actualValue < lowLimit || actualValue > highLimit){
                    editText.setText("0");
                }
            }
            LogMessage.debug("String "+value);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
