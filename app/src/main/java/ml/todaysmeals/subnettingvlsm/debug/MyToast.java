package ml.todaysmeals.subnettingvlsm.debug;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import ml.todaysmeals.subnettingvlsm.Home;

/**
 * Created by GHOSH on 3/19/2017.
 */

public class MyToast {
    public static void Message(String message){
        Toast.makeText(Home.applicationContex,message,Toast.LENGTH_SHORT).show();
    }
}
