package ml.todaysmeals.subnettingvlsm.datastructure;

import java.util.List;

import ml.todaysmeals.subnettingvlsm.classes.SubnetBlock;

/**
 * Created by GHOSH on 3/22/2017.
 */

public class Sorting {
    public static void sortByHostNumber(List<SubnetBlock> subnetBlockList) {
        int highestHostNumber, highestHostNumberIndex;
        int actualHighestHostNumberIndex;
        SubnetBlock temp;
        for (int index = 0; index < subnetBlockList.size(); index++) {
            highestHostNumber = subnetBlockList.get(index).hostNumber;
            highestHostNumberIndex = index;
            actualHighestHostNumberIndex = index;
            for (int i = (index + 1); i < subnetBlockList.size(); i++) {
                if (subnetBlockList.get(i).hostNumber > highestHostNumber) {
                    actualHighestHostNumberIndex = i;
                    highestHostNumber = subnetBlockList.get(i).hostNumber;
                }
            }

            if (actualHighestHostNumberIndex != highestHostNumberIndex) {
                temp = subnetBlockList.get(highestHostNumberIndex);
                subnetBlockList.set(highestHostNumberIndex,subnetBlockList.get(actualHighestHostNumberIndex));
                subnetBlockList.set(actualHighestHostNumberIndex,temp);
            }
        }
    }

    public static void sortBySerialNumber(List<SubnetBlock> subnetBlockList) {
        int lowestSerialNumber, lowestSerialNumberIndex;
        int actualLowestSerialNumberIndex;
        SubnetBlock temp;
        for (int index = 0; index < subnetBlockList.size(); index++) {
            lowestSerialNumber = subnetBlockList.get(index).serialNumber;
            lowestSerialNumberIndex = index;
            actualLowestSerialNumberIndex = index;
            for (int i = (index + 1); i < subnetBlockList.size(); i++) {
                if (subnetBlockList.get(i).serialNumber < lowestSerialNumber) {
                    actualLowestSerialNumberIndex = i;
                    lowestSerialNumber = subnetBlockList.get(i).serialNumber;
                }
            }

            if (actualLowestSerialNumberIndex != lowestSerialNumberIndex) {
                temp = subnetBlockList.get(lowestSerialNumberIndex);
                subnetBlockList.set(lowestSerialNumberIndex,subnetBlockList.get(actualLowestSerialNumberIndex));
                subnetBlockList.set(actualLowestSerialNumberIndex,temp);
            }
        }
    }
}
